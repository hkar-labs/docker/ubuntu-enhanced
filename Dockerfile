FROM ubuntu:18.04

LABEL maintainer="admin@hkar.eu"

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    openssh-client \
    rsync \
 && rm -rf /var/lib/apt/lists/*